# Yoyoga! in AngularJS #

### What is it? ###

Yoyoga! is an AngularJS application that allows you to rate your progress in performing your favourite yoga poses over time. I wrote it in a day as part of a brief foray into learning Angular.

I tried to keep each commit fairly concise so that you can see from one commit to another what the main change was.

** DISCLAIMER: I am in no way an Angular expert, having only looked at it for a few days, so there are probably a number of mistakes and bad practices lurking about. I apologise in advance and feel free to let me know of any that you spot. **

### How do I get set up? ###

1. Clone the repo using ``git clone git@bitbucket.org:danharris293/yoyoga-ang.git``.

2. If you just want to try out the application, then you can use the it in a non-persistent state by running ``git checkout no-persistence`` and skipping to step 5.

3. Otherwise, the app uses Firebase for it's persistence and so in order to use it properly you will need to create a Firebase account (which you can do for free!) and then import the data file ``poses.json`` which can be found in the ``app/data`` directory. This will provide you with three example poses and some dummy progress data.

4. You will then need to copy your Firebase reference URL and paste it into the Firebase constructor: ``var dataRef = new Firebase("<INSERT-FIREBASE-URL-HERE>")`` in both ``app/js/app.js`` and ``app/js/directives.js``.

5. Finally, run ``npm start`` from the top level. Then head to http://localhost:8000/app.
