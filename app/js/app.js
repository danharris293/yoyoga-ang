(function() {
  angular.module("yoyoga", ["directives", "services", "filters", "firebase"])
    .controller("PosesController", ["$firebaseObject", "progressChart", function($firebaseObject, progressChart) {
      this.myPoses = [];
      var dataRef = new Firebase("<INSERT-FIREBASE-URL-HERE>");
      this.myPoses = $firebaseObject(dataRef.child("poses"));
      this.currentPose = null;
      this.currentChart = {};

      this.setCurrentPose = function(pose) {
        this.currentPose = pose;
        this.currentChart = progressChart.create(pose);
      }

      this.isSelected = function(pose) {
        return this.currentPose === pose;
      }
    }]);
})();
