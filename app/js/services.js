(function() {
  angular.module("services", [])
    .service("progressChart", function() {
      var preprocessData = function(data) {
        var result = [];
        for (i in data) {
          result.push([data[i].date, data[i].value]);
        }
        return result;
      };

      this.create = function(pose) {
        if (Highcharts.charts.size > 0) {
          Highcharts.charts[0].destroy();
        }
        Highcharts.chart("progress-chart", {
          chart: {
            type: "line",
            zoomType: "xy"
          },
          title: {
            text: "Progress for " + pose.name
          },
          xAxis: {
            title: {
              enabled: true,
              text: "Date"
            },
            type: "datetime",
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
          },
          yAxis: {
            title: {
              text: "Rating (out of 10)"
            }
          },
          plotOptions: {
            scatter: {
              marker: {
                radius: 5,
                states: {
                  hover: {
                    enabled: true,
                    lineColor: "rgb(100,100,100)"
                  }
                }
              },
              states: {
                hover: {
                  marker: {
                    enabled: false
                  }
                }
              },
              tooltip: {
                pointFormatter: function() {
                  return  "Date: " + Highcharts.dateFormat('%b %e %Y', new Date(this.x))
                    + "<br/>Rating: " + this.y + "/10";
                }
              }
            }
          },
          series: [{
            name: pose.name,
            color: "#002F2F",
            data: preprocessData(pose.progressData)
          }]
        });
      };
    });
})();
