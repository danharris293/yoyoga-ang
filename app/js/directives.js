(function() {
  angular.module("directives", []);

  angular.module("yoyoga")
    .directive("posesList", function() {
      return {
        restrict: "E",
        templateUrl: "templates/poses-list.html"
      };
    });

  angular.module("yoyoga")
    .directive("ratingForm", ["$firebaseArray", function($firebaseArray) {
      return {
        restrict: "E",
        templateUrl: "templates/rating-form.html",
        controller: function() {
          this.rating = {};

          this.addRating = function(pose) {
            var dataRef = new Firebase("<INSERT-FIREBASE-URL-HERE>"),
              progressData =
                $firebaseArray(dataRef.child("poses").child(pose.key).child("progressData"));
            progressData.$add({
              "date": Date.parse(this.rating.date),
              "value": this.rating.value
            });
            this.rating = {};
          };
        },
        controllerAs: "ratingCtrl"
      }
    }]);
})();
