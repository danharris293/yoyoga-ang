(function() {
  angular.module("filters", [])
    .filter("currentDate", ["$filter", function($filter) {
      return function() {
        return $filter("date")(Date.now(), "yyyy-MM-dd");
      };
  }]);

  angular.module("filters")
    .filter("plusOneDay", ["$filter", function($filter) {
      return function(date) {
        return $filter("date")(date + 86400000, "yyyy-MM-dd");
      };
  }]);
})();
